import { NgModule } from '@angular/core';
import { PhotoComponent } from './photo.component';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import {MatFormFieldModule} from '@angular/material/form-field'
import { MatLinkPreviewModule } from '@angular-material-extensions/link-preview';

@NgModule({
    declarations: [PhotoComponent],
    imports: [
        CommonModule,
        HttpClientModule,
        MatLinkPreviewModule,
        MatFormFieldModule
    ],
    exports: [ PhotoComponent ]
})
export class PhotoModule { }